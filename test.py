#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest

from app import app, db
from app.mod_address.models import User, Contact

import json


class TestCase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def prepare_header(self, data):
        headers = [('Content-Type', 'application/json')]
        json_data = json.dumps(data)
        json_data_length = len(json_data)
        headers.append(('Content-Length', json_data_length))
        return headers

    def test_create_user(self):
        u = User(
            name='john',
            surname='Lewsi',
            email='john@example.com',
            password='gfhjkm'
        )
        db.session.add(u)
        db.session.commit()
        user = User.query.filter_by(email='john@example.com').first()
        self.assertFalse(user.verify_password('newpass'))
        self.assertTrue(user.verify_password('gfhjkm'))
        self.assertFalse(User.is_exist_user('newemail@example.com'))
        self.assertTrue(User.is_exist_user('john@example.com'))

    def test_create_contact(self):
        u = User(
            name='john',
            surname='Lewsi',
            email='john@example.com',
            password='gfhjkm'
        )
        db.session.add(u)
        db.session.commit()
        user = User.query.filter_by(email='john@example.com').first()
        self.assertFalse(user.contacts.all())
        c = Contact(
            user=user,
            contact_text="Cool text",
            contact_type='skype'
        )
        db.session.add(c)
        db.session.commit()
        self.assertTrue(user.contacts.all())

    def test_api_create_user(self):
        data = {
            'name': 'Jesse',
            'surname': 'Pinkman',
            'email': 'jesse@example.com',
            'password': 'coolpassword'
        }
        fake_date = {
            'name': 'Jesse',
            'surname': 'Pinkman',
            'email': 'jesse@example.com',
        }
        data = json.dumps(data)
        fake_date = json.dumps(fake_date)
        response = self.app.post(
            '/api/users', headers=self.prepare_header(data), data=fake_date)
        self.assertEqual(response.status_code, 400)
        response = self.app.post(
            '/api/users', headers=self.prepare_header(data), data=data)
        self.assertEqual(response.status_code, 201)
        self.assertTrue(User.query.filter_by(email='jesse@example.com').first())
        response = self.app.post(
            '/api/users', headers=self.prepare_header(data), data=data)
        self.assertEqual(response.status_code, 400)

    def test_api_login_user(self):
        data = {
            'name': 'Jesse',
            'surname': 'Pinkman',
            'email': 'Jesse@example.com',
            'password': 'coolpassword'
        }
        login_data = {
            'email': 'Jesse@example.com',
            'password': 'coolpassword'
        }
        self.app.post(
            '/api/users', headers=self.prepare_header(data),
            data=json.dumps(data)
        )
        resp = self.app.post(
            '/api/login', headers=self.prepare_header(login_data),
            data=json.dumps(login_data)
        )
        self.assertEqual(resp.status_code, 200)
        resp_data = json.loads(resp.data)
        user_token = resp_data.get('token')
        self.assertTrue(user_token)

    def test_api_contacts(self):
        resp = self.app.get('/api/contacts_list')
        self.assertEqual(resp.status_code, 401)
        u = User(
            name='john',
            surname='Lewsi',
            email='john@example.com',
            password='gfhjkm'
        )
        db.session.add(u)
        db.session.commit()
        c = Contact(
            user=u,
            contact_text="my_skype",
            contact_type='skype'
        )
        db.session.add(c)
        db.session.commit()
        c = Contact(
            user=u,
            contact_text="+7111111111",
            contact_type='phone'
        )
        db.session.add(c)
        db.session.commit()
        login_data = {
            'email': 'john@example.com',
            'password': 'gfhjkm'
        }
        resp = self.app.post(
            '/api/login', headers=self.prepare_header(login_data),
            data=json.dumps(login_data)
        )
        user_token = json.loads(resp.data).get('token')
        user_headers = {
            'Authorization': user_token,
            'Content-Type': 'application/json'
        }
        resp = self.app.get('/api/contacts_list', headers=user_headers)
        resp_data = json.loads(resp.data)
        self.assertTrue(resp_data.get('success'))
        self.assertEqual(
            resp_data.get('data')[0].get('name'),
            'john'
        )
        #Personal contacts check
        data = {
            'name': 'Jesse',
            'surname': 'Pinkman',
            'email': 'Jesse@example.com',
            'password': 'coolpassword'
        }
        login_data = {
            'email': 'Jesse@example.com',
            'password': 'coolpassword'
        }
        self.app.post(
            '/api/users', headers=self.prepare_header(data),
            data=json.dumps(data)
        )
        resp = self.app.post(
            '/api/login', headers=self.prepare_header(login_data),
            data=json.dumps(login_data)
        )
        jess_token = json.loads(resp.data).get('token')
        contact_data = {
            'text': 'jess_skype',
            'type': 'skype'
        }
        jess_headers = {
            'Authorization': jess_token,
            'Content-Type': 'application/json'
        }
        contact_error_data = {
            'text': 'jess_skype',
            'type': 'skyp'
        }
        resp = self.app.post(
            '/api/contacts', headers=jess_headers,
            data=json.dumps(contact_error_data)
        )
        self.assertEqual(resp.status_code, 400)
        resp = self.app.post(
            '/api/contacts', headers=jess_headers,
            data=json.dumps(contact_data)
        )
        self.assertEqual(resp.status_code, 201)
        resp = self.app.get(
            '/api/contacts', headers=jess_headers,
        )
        self.assertEqual(resp.status_code, 200)
        jess_data = json.loads(resp.data)
        self.assertTrue(jess_data.get('success'))
        self.assertEqual(
            len(jess_data.get('data').get('contacts')),
            1
        )
        resp = self.app.get(
            '/api/contacts', headers=user_headers,
        )
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(resp_data.get('success'))
        john_data = json.loads(resp.data)
        self.assertTrue(john_data.get('success'))
        self.assertEqual(
            len(john_data.get('data').get('contacts')),
            2
        )
        contact_id = john_data.get('data').get('contacts')[0].get('id')
        contact_jesse_id = jess_data.get('data').get('contacts')[0].get('id')
        resp = self.app.delete(
            '/api/contacts', headers=user_headers,
            data=json.dumps({'id': contact_jesse_id})
        )
        self.assertEqual(resp.status_code, 400)
        resp = self.app.delete(
            '/api/contacts', headers=user_headers,
            data=json.dumps({'id': contact_id})
        )
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(Contact.query.all()), 2)
        resp = self.app.put(
            '/api/contacts', headers=jess_headers,
            data=json.dumps({
                'id': contact_jesse_id,
                'text': 'jess_new_skype'
            })
        )
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(
            Contact.query.get(contact_jesse_id).contact_text,
            'jess_new_skype'
        )

    def test_user_search(self):
            resp = self.app.get('/api/search')
            self.assertEqual(resp.status_code, 401)
            u = User(
                name='john',
                surname='Lewsi',
                email='john@example.com',
                password='gfhjkm'
            )
            db.session.add(u)
            db.session.commit()
            c = Contact(
                user=u,
                contact_text="my_skype",
                contact_type='skype'
            )
            db.session.add(c)
            db.session.commit()
            c = Contact(
                user=u,
                contact_text="+7111111111",
                contact_type='phone'
            )
            db.session.add(c)
            db.session.commit()
            u = User(
                name='Jesse',
                surname='Lewsi',
                email='jess@example.com',
                password='gfchcsds'
            )
            db.session.add(u)
            db.session.commit()
            c = Contact(
                user=u,
                contact_text="jess_skype",
                contact_type='skype'
            )
            db.session.add(c)
            db.session.commit()
            login_data = {
                'email': 'john@example.com',
                'password': 'gfhjkm'
            }
            resp = self.app.post(
                '/api/login', headers=self.prepare_header(login_data),
                data=json.dumps(login_data)
            )
            user_token = json.loads(resp.data).get('token')
            user_headers = {
                'Authorization': user_token,
                'Content-Type': 'application/json'
            }
            resp = self.app.get(
                '/api/search',
                headers=user_headers,
                data=json.dumps({'q': 'J'})
            )
            self.assertEqual(resp.status_code, 200)
            self.assertFalse(json.loads(resp.data).get('data'))
            resp = self.app.get(
                '/api/search',
                headers=user_headers,
                data=json.dumps({'q': 'ess'})
            )
            self.assertEqual(
                len(json.loads(resp.data).get('data')),
                1
            )

if __name__ == '__main__':
    unittest.main()
