ContactBookApp.factory('authInterceptor', function ($rootScope, $q, $window) {
  return {
    request: function (config) {
      config.headers = config.headers || {};
      if ($window.sessionStorage.token) {
        config.headers.Authorization = $window.sessionStorage.token;
      }
      return config;
    },
    responseError: function(rejection) {
      if (rejection.status === 401) {
        $('#authModal').modal('show');
      }
      return rejection || $q.when(rejection);
    }
  };
});

ContactBookApp.config(function ($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
  $httpProvider.defaults.headers["delete"] = {'Content-Type': 'application/json;charset=utf-8'};
});
