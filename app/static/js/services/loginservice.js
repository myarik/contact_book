'use strict';

ContactBookApp.factory('AuthService', ["$q", "$window", "$rootScope", '$http', function($q, $window, $rootScope, $http) {

    var login_url = "/api/login";
    var registration_url = "/api/users";
    return {
        registration: function (user) {
            var defer = $q.defer();
            $http.post(registration_url, user).
                success(function (data, status, headers, config) {
                    defer.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    defer.reject(status);
                });
            return defer.promise;
        },
        login: function(user) {
            var defer = $q.defer();
            $http.post(login_url, user).
                success(function (data, status, headers, config) {
                    if (status==200){
                        $window.sessionStorage.token = data.token;
                        defer.resolve(data);
                    };
                    defer.reject(status);
                })
                .error(function (data, status, headers, config) {
                    delete $window.sessionStorage.token;
                    defer.reject(status);
                });
            return defer.promise;
        },
        logoutUser: function () {
            delete $window.sessionStorage.token;
        },
    }

}]);
