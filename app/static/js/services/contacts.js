'use strict';

ContactBookApp.factory('ContactService', ["$q", "$window", "$rootScope", '$http', function($q, $window, $rootScope, $http) {

    var url = "/api/contacts";
    var url_list = "/api/contacts_list";
    return {
        add: function (contact) {
            var defer = $q.defer();
            $http.post(url, contact).
                success(function (data, status, headers, config) {
                    defer.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    defer.reject(status);
            });
            return defer.promise;
        },
        list: function () {
            var defer = $q.defer();
            $http.get(url_list).
                success(function (data, status, headers, config) {
                    defer.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    defer.reject(status);
            });
            return defer.promise;
        },
        get: function () {
            var defer = $q.defer();
            $http.get(url).
                success(function (data, status, headers, config) {
                    defer.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    defer.reject(status);
            });
            return defer.promise;
        },
        del: function (contact) {
            var defer = $q.defer();
            $http({url: url, method: 'DELETE', data: contact}).
                success(function (data, status, headers, config) {
                    defer.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    defer.reject(status);
            });
            return defer.promise;
        },
    }

}]);
