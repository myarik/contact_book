'use strict';

ContactBookApp.controller('MainCtrl', function($scope, $window, $log, AuthService){
    $scope.alerts = [];
    if(typeof $window.sessionStorage.token === 'undefined'){
      $scope.auth = true;
    }else{
      $scope.auth = false;
    };
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.login = function(user) {
      AuthService.login(user).then(function(response) {
          $scope.alerts.push({type: 'success', msg: 'Thank you for login!'});
          $scope.auth = false
      }, function (response) {
          $scope.alerts.push({type: 'danger', msg: 'Sory, please check data'});
      });
    };
    $scope.isAuthShow = function() {
        return $scope.auth
    };
    $scope.isLogoutShow = function() {
        if ($scope.auth) {
          return false
        };
        return true
    };
    $scope.logout = function() {
      AuthService.logoutUser()
      $scope.alerts.push({type: 'success', msg: 'Sign out!'});
      $scope.auth = true;
    };
});

ContactBookApp.controller('RegistrationCtrl', function($scope, $log, $location, AuthService){
    $scope.alerts = [];
    $scope.closeAlert = function(index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.addUser = function(user) {
      AuthService.registration(user).then(function(response) {
          $scope.alerts.push({type: 'success', msg: 'Thank you for registration!'});
          $location.path('/');
      }, function (response) {
          $scope.alerts.push({type: 'danger', msg: 'Sory, please check data'});
      });
    };
});

ContactBookApp.controller('ContactCtrl', function($scope, $http, $log, ContactService){
    $scope.tab = 1;
    function get_list() {
      ContactService.list().then(function(response) {
          $scope.users = response.data;
      });
    }
    get_list();
    ContactService.get().then(function(response) {
        $scope.my_info = response.data;
    });
    $scope.selectTab = function(setTab){
      $scope.tab = setTab;
    };
    $scope.isSelect = function(checkTab){
      return $scope.tab === checkTab;
    };
    $scope.addContact = function(new_contact) {
      ContactService.add(new_contact).then(function(response) {
        $scope.my_info.contacts.push(response);
        get_list();
        $scope.new_contact.type ='';
        $scope.new_contact.text = '';
      });
    };
    $scope.delContact = function(index, contact) {
      ContactService.del(contact).then(function(response) {
        $scope.my_info.contacts.splice(index, 1);
        get_list();
      });
    };
});

var AuthModal = function ($scope, $route, $window, AuthService) {
  $scope.login = function(user) {
      AuthService.login(user).then(function(response) {
        $window.location.reload();
      }, function (response) {
          $window.location.reload();
      });
  };
};
