'use strict';

var ContactBookApp = angular.module("ContactBookApp", [
    'ngRoute',
    'ngCookies',
    'ui.bootstrap'
]);

ContactBookApp.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: 'static/js/views/main.html'
        })
        .when("/registration", {
            templateUrl: 'static/js/views/registration.html'
        })
        .when("/contacts", {
            templateUrl: 'static/js/views/contacts.html'
        })
        .otherwise({
            redirectTo: '/'
        })
})
