# -*- coding: utf-8 -*-
import random
import string


def generate_salt(length=8):
    """
        Generate random salt
    """
    letters_digits = string.ascii_letters + string.digits
    return ''.join(random.sample(letters_digits, length))
