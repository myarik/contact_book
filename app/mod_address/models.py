# -*- coding: utf-8 -*-
from app import db, app
from app.mod_address.utils import generate_salt

from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (
    TimedJSONWebSignatureSerializer as Serializer,
    BadSignature,
    SignatureExpired
)


# Define a base model for other database tables to inherit
class Base(db.Model):

    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)


class User(Base):
    __tablename__ = 'user'
    # Email
    email = db.Column(db.String(128),  nullable=False, unique=True)
    # User Name
    name = db.Column(db.String(256),  nullable=False)
    # User Surname
    surname = db.Column(db.String(256),  nullable=False)
    # User Salt
    salt = db.Column(db.String(256),  nullable=False)
    # User Password
    password = db.Column(db.String(192),  nullable=False)

    def __init__(self, email, name, surname, password):
        self.email = email.lower()
        self.name = name
        self.surname = surname
        # generate user salt
        self.salt = generate_salt()
        # save password hash
        self.password = self.hash_password(password, self.salt)

    def __repr__(self):
        return '<User %r>' % (self.name)

    def hash_password(self, password, salt):
        """
            Create password hash
        """
        pwd = password + salt
        return pwd_context.encrypt(pwd)

    def verify_password(self, password):
        """
            Check password
        """
        pwd = password + self.salt
        return pwd_context.verify(pwd, self.password)

    @staticmethod
    def is_exist_user(email):
        """
            Check if user is exist
        """
        if User.query.filter_by(email=email.lower()).first():
            return True
        return False

    def generate_auth_token(self, expiration=60*60*48):
        """
           Generate Token on 2 days
        """
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def get_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None
        except BadSignature:
            return None  # invalid token
        user = User.query.get(data['id'])
        return user

    @property
    def serialize(self):
        """
            Return object data in easily serializeable format
        """
        return {
            'id': self.id,
            'name': self.name,
            'surname': self.surname,
            'contacts': self.serialize_contacts
        }

    @property
    def serialize_contacts(self):
        """
            Return contacts
        """
        return [item.serialize for item in self.contacts.all()]


class Contact(Base):
    __tablename__ = 'contact'

    CONTACT_TYPE = ('phone', 'email', 'skype')

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User',
                           backref=db.backref('contacts', lazy='dynamic'))
    contact_text = db.Column(db.Text, nullable=True)
    contact_type = db.Column(db.Enum(*CONTACT_TYPE), nullable=False)

    def __init__(self, user, contact_text, contact_type):
        assert contact_type in self.CONTACT_TYPE
        self.contact_text = contact_text
        self.user = user
        self.contact_type = contact_type

    def __repr__(self):
        return '<Contact %r>' % (self.contact_type)

    @property
    def serialize(self):
        """
            Return object data in easily serializeable format
        """
        return {
            'id': self.id,
            'text': self.contact_text,
            'type': self.contact_type
        }

    def is_user_contact(self, user):
        """
            Check if user is contact's owner
        """
        return self.user == user
