# -*- coding: utf-8 -*-
from flask import jsonify, request, Response, abort, g

from app import app, db

from .models import User, Contact

from functools import wraps


def login_required(func):
    '''
    If you decorate a view with this, it will ensure that the current user
    is sent auth token before calling the actual view.
    If user not sent or token incorrect, it will return 404
    :param func: The view function to decorate.
    :type func: function
    '''
    @wraps(func)
    def decorated_view(*args, **kwargs):
        auth = request.headers.get('Authorization', None)
        if auth:
            user = User.get_token(auth)
            if user:
                g.user = user
                return func(*args, **kwargs)
        return abort(401)
    return decorated_view


@app.route('/api/users', methods=['POST'])
def new_user():
    """
        API create User
        :return: status code 400 - User exist or field incorrect
    """
    email = request.json.get('email')
    password = request.json.get('password')
    surname = request.json.get('surname')
    name = request.json.get('name')
    if email is None or password is None or surname is None or name is None:
        abort(400)
    if User.is_exist_user(email):
        abort(400)
    user = User(
        name=name,
        surname=surname,
        email=email,
        password=password
    )
    db.session.add(user)
    db.session.commit()
    return jsonify({'username': user.name}), 201


@app.route('/api/login', methods=['POST'])
def login_user():
    """
        API create User
        :return: status code 400 - incorrect value
    """
    email = request.json.get('email')
    password = request.json.get('password')
    if email is None:
        return jsonify({'success': False, 'error_field': 'email'}), 201
    if password is None:
        return jsonify({'success': False, 'error_field': 'password'}), 201
    user = User.query.filter_by(email=email.lower()).first()
    if not user or not user.verify_password(password):
        abort(400)
    token = user.generate_auth_token()
    return jsonify({'token': token.decode('ascii')})


@app.route('/api/contacts_list', methods=['GET'])
@login_required
def contact_list():
    """
        API create User
        :return: status code 401 - user not auth
    """
    data_respond = []
    for user in User.query.all():
        data_respond.append(user.serialize)
    return jsonify({'success': True, 'data': data_respond})


@app.route('/api/contacts', methods=['GET', 'POST', 'DELETE', 'PUT'])
@login_required
def contact():
    """
        API create User
        :return: status code 401 - user not auth
        :return: status code 400 - field incorrect or you not contact owner
    """
    if request.method == 'POST':
        contact_text = request.json.get('text')
        contact_type = request.json.get('type')
        if contact_text is None or contact_type is None:
            abort(400)
        try:
            contact = Contact(
                user=g.user,
                contact_text=contact_text,
                contact_type=contact_type
            )
        except Exception:
            abort(400)
        db.session.add(contact)
        db.session.commit()
        context_dict = {
            'id': contact.id,
            'text': contact.contact_text,
            'type': contact.contact_type
        }
        return jsonify(context_dict), 201
    if request.method == 'GET':
        return jsonify({'success': True, 'data': g.user.serialize})
    contact_id = request.json.get('id')
    if contact_id is None:
        abort(400)
    contact = Contact.query.filter_by(id=contact_id).first()
    if not contact or not contact.is_user_contact(g.user):
        abort(400)
    if request.method == 'DELETE':
        db.session.delete(contact)
        db.session.commit()
        return jsonify({'success': True})
    if request.method == 'PUT':
        contact_text = request.json.get('text')
        contact_type = request.json.get('type')
        if contact_text:
            contact.contact_text = contact_text
        if contact_type:
            contact.contact_type = contact_type
        try:
            db.session.commit()
        except Exception:
            abort(400)
        return jsonify({'success': True, 'data': contact.serialize})


@app.route('/api/search', methods=['GET'])
@login_required
def search():
    """
        API create User
        :return: status code 401 - user not auth
    """
    search = request.json.get('q')
    data_respond = []
    if len(search) < 3:
        return jsonify({'success': True, 'data': data_respond})
    for user in User.query.filter(
            User.name.like("%{}%".format(search))).all():
        data_respond.append(user.serialize)
    for user in User.query.filter(
            User.surname.like("%{}%".format(search))).all():
        data_respond.append(user.serialize)
    return jsonify({'success': True, 'data': data_respond})
